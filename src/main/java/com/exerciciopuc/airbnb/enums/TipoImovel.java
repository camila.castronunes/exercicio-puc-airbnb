package com.exerciciopuc.airbnb.enums;

public enum TipoImovel {
    APARTAMENTO, CASA, QUARTO;
}
