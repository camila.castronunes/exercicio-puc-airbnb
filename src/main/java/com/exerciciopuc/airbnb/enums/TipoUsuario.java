package com.exerciciopuc.airbnb.enums;

public enum TipoUsuario {
    LOCADOR, LOCATARIO;
}
