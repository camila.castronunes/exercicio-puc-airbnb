package com.exerciciopuc.airbnb.services;


import com.exerciciopuc.airbnb.dto.UsuarioDto;
import com.exerciciopuc.airbnb.entities.Usuario;
import com.exerciciopuc.airbnb.exceptions.NaoEncontradoException;
import com.exerciciopuc.airbnb.repositories.UsuarioRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Transactional(readOnly = true)
    public List<UsuarioDto> buscaTodos() {
        List<Usuario> listaEntity = usuarioRepository.findAll();

        return listaEntity.stream().map(aux -> new UsuarioDto(aux)).collect(Collectors.toList());
    }

    @Transactional
    public UsuarioDto add(UsuarioDto usuarioDto) {
        Random geradorSenha = new Random();
        Usuario usuario = new Usuario();
        usuario.setLogin(usuarioDto.getLogin());
        usuario.setStatus(usuarioDto.getStatus());
        usuario.setTipo(usuarioDto.getTipo());
        usuario.setSenha(String.valueOf(geradorSenha.nextInt()));
        usuario = usuarioRepository.save(usuario);
        return new UsuarioDto(usuario);
    }


    @Transactional(readOnly = true)
    public UsuarioDto buscaPorLogin(String login) {
        Usuario usuarioEntity = buscaEvalidaLogin(login);
        UsuarioDto usuarioDto = new UsuarioDto(usuarioEntity);
        return usuarioDto;
    }

    @Transactional
    public UsuarioDto editaUsuario(String login, UsuarioDto usuarioDto) {

        Usuario usuarioEntity = buscaEvalidaLogin(login);

        BeanUtils.copyProperties(usuarioDto, usuarioEntity, "login");

        usuarioEntity = usuarioRepository.save(usuarioEntity);
        return new UsuarioDto(usuarioEntity);

    }

    @Transactional
    public void delete(String login) {
        Usuario usuario = buscaEvalidaLogin(login);
        usuarioRepository.delete(usuario);


    }


    private Usuario buscaEvalidaLogin(String login) {
        Usuario usuario = usuarioRepository.findById(login).orElseThrow(() -> new NaoEncontradoException("Login de usuário não encontrado"));


        return usuario;
    }
}
