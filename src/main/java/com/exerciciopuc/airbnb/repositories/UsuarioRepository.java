package com.exerciciopuc.airbnb.repositories;

import com.exerciciopuc.airbnb.entities.Usuario;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UsuarioRepository extends MongoRepository<Usuario,String> {

    public Usuario findByLogin(String login);


}
