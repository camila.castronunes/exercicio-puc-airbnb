package com.exerciciopuc.airbnb.dto;

import com.exerciciopuc.airbnb.entities.Usuario;
import com.exerciciopuc.airbnb.enums.TipoUsuario;

public class UsuarioDto {

    private String login;
    private TipoUsuario tipo;
    private Boolean status;

    public UsuarioDto(String login, TipoUsuario tipo, Boolean status) {
        this.login = login;
        this.tipo = tipo;
        this.status = status;
    }

    public UsuarioDto() {
    }

    public UsuarioDto(Usuario usuario) {
        this.login = usuario.getLogin();
        this.tipo = usuario.getTipo();
        this.status = usuario.getStatus();
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public TipoUsuario getTipo() {
        return tipo;
    }

    public void setTipo(TipoUsuario tipo) {
        this.tipo = tipo;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
}
