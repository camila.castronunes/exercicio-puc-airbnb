package com.exerciciopuc.airbnb.exceptions;

public class NaoEncontradoException extends RuntimeException{
    public NaoEncontradoException (String msg){
        super(msg);}

}
