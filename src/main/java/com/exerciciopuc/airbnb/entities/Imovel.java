package com.exerciciopuc.airbnb.entities;

import com.exerciciopuc.airbnb.enums.TipoImovel;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "imoveis")
public class Imovel {
    @Id
    private String idImovel;
    private String nomeImovel;
    private TipoImovel tipoImovel;
    private String cidade;
    private Long precoAluguel;

    public Imovel(String idImovel, String nomeImovel, TipoImovel tipoImovel, String cidade, Long precoAluguel) {
        this.nomeImovel = nomeImovel;
        this.tipoImovel = tipoImovel;
        this.cidade = cidade;
        this.precoAluguel = precoAluguel;
    }

    public Imovel() {
    }

    public String getIdImovel() {
        return idImovel;
    }

    public void setIdImovel(String idImovel) {
        this.idImovel = idImovel;
    }

    public String getNomeImovel() {
        return nomeImovel;
    }

    public void setNomeImovel(String nomeImovel) {
        this.nomeImovel = nomeImovel;
    }

    public TipoImovel getTipoImovel() {
        return tipoImovel;
    }

    public void setTipoImovel(TipoImovel tipoImovel) {
        this.tipoImovel = tipoImovel;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public Long getPrecoAluguel() {
        return precoAluguel;
    }

    public void setPrecoAluguel(Long precoAluguel) {
        this.precoAluguel = precoAluguel;
    }
}
