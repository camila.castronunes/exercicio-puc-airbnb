package com.exerciciopuc.airbnb.entities;


import com.exerciciopuc.airbnb.enums.TipoUsuario;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "usuarios")
public class Usuario {

    @Id
    private String login;
    private String senha;
    private TipoUsuario tipo;

    private Boolean status;

    public Usuario() {
    }

    public Usuario(String login, String senha, TipoUsuario tipo, Boolean status) {
        this.login = login;
        this.senha = senha;
        this.tipo = tipo;
        this.status = status;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public TipoUsuario getTipo() {
        return tipo;
    }

    public void setTipo(TipoUsuario tipo) {
        this.tipo = tipo;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
}