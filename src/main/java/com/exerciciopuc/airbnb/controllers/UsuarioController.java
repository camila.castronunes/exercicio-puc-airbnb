package com.exerciciopuc.airbnb.controllers;


import com.exerciciopuc.airbnb.dto.UsuarioDto;
import com.exerciciopuc.airbnb.entities.Usuario;
import com.exerciciopuc.airbnb.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/usuarios")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;


    @GetMapping
    public ResponseEntity<List<UsuarioDto>> findAll(){
        return ResponseEntity.ok().body(usuarioService.buscaTodos());
    }

    @GetMapping("/{login}")
    public ResponseEntity<UsuarioDto> buscaPorLogin(@PathVariable String login) {
        return ResponseEntity.ok().body(usuarioService.buscaPorLogin(login));
    }


    @PostMapping
    public ResponseEntity<UsuarioDto> addUsuario(@RequestBody UsuarioDto usuarioDto ) {
        return ResponseEntity.status(HttpStatus.CREATED).body(usuarioService.add(usuarioDto));
    }


    @PutMapping("edit/{login}")
    public ResponseEntity <UsuarioDto> editaUsuario(@PathVariable String login, @RequestBody UsuarioDto usuarioDto){
        return ResponseEntity.ok().body(usuarioService.editaUsuario(login,usuarioDto));
    }


    @DeleteMapping("delete/{login}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletaUsuario(@PathVariable String login){
    usuarioService.delete(login);
    }


}
